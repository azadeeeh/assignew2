json.array!(@individials) do |individial|
  json.extract! individial, :id, :name, :weight, :height, :color, :education, :nationality
  json.url individial_url(individial, format: :json)
end
