class IndividialsController < ApplicationController
  before_action :set_individial, only: [:show, :edit, :update, :destroy]

  # GET /individials
  # GET /individials.json
  def index
    @individials = Individial.all
  end

  # GET /individials/1
  # GET /individials/1.json
  def show
  end

  # GET /individials/new
  def new
    @individial = Individial.new
  end

  # GET /individials/1/edit
  def edit
  end

  # POST /individials
  # POST /individials.json
  def create
    @individial = Individial.new(individial_params)

    respond_to do |format|
      if @individial.save
        format.html { redirect_to @individial, notice: 'Individial was successfully created.' }
        format.json { render :show, status: :created, location: @individial }
      else
        format.html { render :new }
        format.json { render json: @individial.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /individials/1
  # PATCH/PUT /individials/1.json
  def update
    respond_to do |format|
      if @individial.update(individial_params)
        format.html { redirect_to @individial, notice: 'Individial was successfully updated.' }
        format.json { render :show, status: :ok, location: @individial }
      else
        format.html { render :edit }
        format.json { render json: @individial.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /individials/1
  # DELETE /individials/1.json
  def destroy
    @individial.destroy
    respond_to do |format|
      format.html { redirect_to individials_url, notice: 'Individial was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_individial
      @individial = Individial.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def individial_params
      params.require(:individial).permit(:name, :weight, :height, :color, :education, :nationality)
    end
end
