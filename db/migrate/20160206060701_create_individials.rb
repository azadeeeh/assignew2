class CreateIndividials < ActiveRecord::Migration
  def change
    create_table :individials do |t|
      t.string :name
      t.float :weight
      t.float :height
      t.string :color
      t.string :education
      t.string :nationality

      t.timestamps null: false
    end
  end
end
